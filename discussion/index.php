<!-- Make code.php file usable (module concept) -->
<?php require_once "./code.php"?>


<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S01 : PHP Basics and Selection Control Structure</title>

	</head>
	<body>
			<h1>Echoing Values</h1>

			<!-- Using variable in HTML code ($variableName) -->
			<p> <?php echo $trail ?> </p>

			<!-- Double quote is like template literals but single quote is pure string -->
			<p> <?php echo "Good day $name! your given email is $email.PI".PI; ?> </p>
			<p> <?php echo 'Good day $name! your given email is $email.'; ?> </p>

			<!-- Use constants value (variableName) -->
			<p> <?php echo PI?> </p> 
			<!-- Use variable value ($varaibleName) -->
			<p> <?php echo $name?> </p>

			<h1>Data Types</h1>
			<h3>String</h3>
			<p><?php echo $state ?></p>
			<p><?php echo $country ?></p>
			<p><?php echo $address ?></p>

			<h3>Integer</h3>
			<p><?php echo $age ?></p>
			<p><?php echo $headcount ?></p>

			<h3>Float</h3>
			<p><?php echo $grade ?></p>
			<p><?php echo $distanceInKm ?></p>

			<h3>Boolean</h3>
			<!-- Normal echoing of boolean variables will not make it visible in web page -->
			<p><?php echo var_dump($hasTraveledAbraod) ?></p>
			<p><?php echo var_dump($haveSymptoms) ?></p>

			<h3>Null</h3>
			<!-- Normal echoing of boolean variables will not make it visible in web page -->
			<p><?php echo var_dump($spouse) ?></p>
			<p><?php echo $middle ?></p>

			<h3>Arrays</h3>
			<p><?php echo $grades[3] ?></p>
			<p><?php echo print_r($grades) ?></p>

			<h3>Objects</h3>
			<p><?php echo $grades[3] ?></p>
			<p><?php echo print_r($gradesObj) ?></p>
			<p><?php echo $gradesObj ->firstGrading ?></p>
			<p><?php echo $personObj ->address ->state ?></p>
			<p><?php echo $personObj ->contact[0] ?></p>

			<h3>gettype</h3>
			<p><?php echo gettype($state) ?></p>
			<p><?php echo gettype($grade) ?></p>

			<h1>Operators</h1>
			<h3>Assignment Operator</h3>
			<p>X: <?php echo $y ?></p>
			<p>Y: <?php echo $x ?></p>

			<p>Is Legal Age: <?php echo var_dump($isLegalAge) ?></p>
			<p>Is Registered: <?php echo var_dump($isRegistered) ?></p>

			<h3>Arithmetic Operator</h3>
			<p>Sum: <?php echo $x + $y ?></p>
			<p>Product: <?php echo $x * $y ?></p>
			<p>Difference: <?php echo $x - $y ?></p>
			<p>Quotient: <?php echo $x / $y ?></p>

			<h3>Equality and Inequality Operator</h3>
			<!-- Not strict on data type -->
			<p>Loose Equality: <?php echo var_dump($x == '1342.14') ?></p>
			<p>Loose Inequality: <?php echo var_dump($x != '1342.14') ?></p>
			<!-- data type matter -->
			<p>Strict Equality: <?php echo var_dump($x === '1342.14') ?></p>
			<p>Strict Inequality: <?php echo var_dump($x !== '1342.14') ?></p>

			<h3>Greater or Lesser Operator</h3>
			<p>Is Lesser: <?php echo var_dump($x < $y) ?></p>
			<p>Is Greater: <?php echo var_dump($x > $y) ?></p>
			<p>Is Lesser or Equal: <?php echo var_dump($x <= $y) ?></p>
			<p>Is Greater or Equal: <?php echo var_dump($x >= $y) ?></p>

			<h3>Logical Operator</h3>
			<!-- Verify whether an expression or group of expression are either true or false -->
			<!-- true if one of expression is true -->
			<p>OR : <?php echo var_dump($isLegalAge || $isRegistered) ?></p>
			<!-- true if all of expression is true -->
			<p>AND : <?php echo var_dump($isLegalAge && $isRegistered) ?></p>
			<p>NOT operator : <?php echo var_dump(!$isLegalAge) ?></p>

			<h1>Function</h1>
			<!-- If you only pass 2 parameter (lessthan needed) if JS its undefined in PHP its an error -->
			<!-- If you pass 3 parameter (morethan needed) if JS its and PHP no error -->

			<p> <?php echo getFullName('John', 'D.', 'Smith') ?> </p>

			<h1>Selection Control Structures</h1>

			<h3>If-Else-If Statement</h3>
			<p> <?php echo determineTyphoonIntensity(39); ?> </p>

			<h3>Ternary Operator</h3>
			<p>78: <?php echo var_dump(isUnderAge(78)); ?> </p>
			<!-- Still works without echo -->
			<p>78: <?php var_dump(isUnderAge(17)); ?> </p>

			<h3>Switch Statement</h3>
			<p>User: <?php echo determineComputerUser(2); ?> </p>

			<h3>Try Catch Finally Statement</h3>
			<p>Greeting <?php echo greeting(12); ?> </p>



	</body>
</html>