<?php 

// Strict use of ; to end statement. PHP is strict in using semicolons (;) as statement terminators.
// html = not sensitive with ;
// php = strict with ;
$trail = 'Hello World';

// Comments 
// Single line (//)
// Multi-line (/**/)


// Variables
// Used to contain data 
// Syntax: $variableName = data

$name = 'John Smith';
$email = 'johnsmith@gmail.com';

// Constants 
// Constants used to hold data that are meant read-only.
// Syntax:
	// define('variableName', valueOfVariable)

define('PI', 3.1416);

// Reassignment of a variable 
$name = 'Will Smith';


// Data Types

// String
$state = 'New York';
$country = 'USA';
// In PHP, the dot (.) is used as the concatenation operator to combine two strings together.
$address = $state. ', '.$country.PI;

// Reassign the address
// $address = "$state, $country"

// Integer
$age = 31;
$headcount = 26;

// Floats (Numbers with decimal)
$grade = 98.2;
$distanceInKm = 1342.12;


// Boolean
$hasTraveledAbraod = false;
$haveSymptoms = true;


// Null 
$spouse = null;
$middle = null;

// Array
$grades = array(98.7, 92.1, 90.2, 94.6);

//Objects
$gradesObj = (object)[
	'firstGrading' => 98.7,
	'secondGrading' => 92.1,
	'thirdGrading' => 90.2,
	'fourthGrading' => 94.6
];



$personObj = (object)[
	'fullName' => 'John Smith',
	'isMarried' => false,
	'age' => 35,
	'address' => (object)[
			'state' => 'New York',
			'country' => 'USA'
		],
	'contact' => array('09123456789', '09111111111')	
];

// Operators
// Assignment operators 
// Used to assign and reassign values of a variable

$x = 1342.14;
$y = 1234.56;

$isLegalAge = true;
$isRegistered = false;

// Functions
// Used to make reusable code


function getFullName($firstName, $middleName, $lastName) {
	// you can add your algorith here

	return "$lastName, $firstName $middleName";
}


// Selection Control Structures
// Used to make code execution dynamic according to predefined conditions
	// if-elseif-else condition
	
	function determineTyphoonIntensity($windSpeed) {

		if($windSpeed <30) {
			return 'Not a typhoon';
		} else if ($windSpeed <= 60) {
			return 'Tropical depression detected';
		} elseif ($windSpeed <= 88) {
			return 'Tropical storm detected';
		} elseif ($windSpeed <= 117) {
			return 'Severe tropical storm detected';
		} else {
			return 'Typhoon detected';
		}
	}

// Conditional (Ternary) Operator

	function isUnderAge($age) {
		return ($age < 18) ? true : false;
	}

// Switch Statement

	function determineComputerUser($computerNumber){
		switch ($computerNumber) {
			case 1:
				return 'Linus Torvald';
				break;
			case 2:
				return 'Steve Jobs';
				break;	
			case 3:
				return 'Sid Meir';
				break;
			case 4:
				return 'Onel de Guzman';
				break;
			case 1:
				return 'Christian Salvador';
				break;
			default:
				return $computerNumber. 'is out of bounds.';
				break;
		}
	}

// Try-Catch-Finally
	function greeting($str) {
		try {
			// Try will attempt to execute code
			if(gettype($str) == "string"){
				// If successful
				echo $str;
			} else {
				// If not successful
				throw new Exception("Oops!");
			}
		}
		// Catch the errors within try
		// e refers to error
		catch (Exception $e){
			// getMessage is method inside $e object
			echo $e->getMessage();
		}
		// Despite errors in try finally will always execute
		finally {
			echo "I did it again!";
		}
	}

?>