<?php 

function getFullAddress($country, $city, $province, $specificAddress){
	return $specificAddress. ', ' .$city. ', ' .$province. ', ' .$country;
}


function getLetterGrade($grade) {
	if ($grade >= 98 && $grade < 101) {
		return 'A+';
	} elseif ($grade >= 95 && $grade < 98){
		return 'A';
	} elseif ($grade >= 92 && $grade < 95){
		return 'A-';
	} elseif ($grade >= 89 && $grade < 92){
		return 'B+';
	} elseif ($grade >= 86 && $grade < 89){
		return 'B';
	} elseif ($grade >= 83 && $grade < 86){
		return 'B-';
	} elseif ($grade >= 80 && $grade < 83){
		return 'C+';
	} elseif ($grade >= 77 && $grade < 80){
		return 'C';
	} elseif ($grade >= 75 && $grade < 77){
		return 'C-';
	} elseif ($grade < 75){
		return 'F';
	} else {
		return $grade. 'Please check inputed grade';
	}
}


?>


